package com.fliife.befunge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.fliife.befunge.utils.CodeSavingUtils;

public class MainActivity extends Activity 
{
    public static int SAMPLE_RESULT = 1;
    public static int RESULT_RELOAD = 2;
    public String defaultPrograms = "CgVRdWluZQoNUmV2ZXJzZSBxdWluZQoIRml6ekJ1enoKAm4hCgMyXm4SDzow" +
            "Zzo4NCotISNAXywxKxIOIj4sOiMgXzY2KjItLEAStQEwPjErOjMlI3ZfInp6aWYiLCwsLCAgICAgdj46NSUj" +
            "dl8ienp1YiIsLCwsIHYKICwgICAgICA+ICAgICAgICAgICAgICAgICBeICAgID4gICAgICAgICAgIHYKIF4t" +
            "NCo2NjwgICAgICAgICAgIDwgICAgICAgICAgICAgICAgICAgIDwuOjwKICAgICAgIF4sLCwsImJ1enoiX14j" +
            "ISU1OjwgICAgICAgICAgICAgIF4gICA8EhwmPjojIDEjIC0jIDojIF8kPlwjIDojKiBfJC5AEh4mMS0yPjIj" +
            "ICojIFwjIDEjIC0jICNcIDojIF8kLkA=";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(prefs.getBoolean("firstStart", true)) prefs.edit().putString("saved_code", defaultPrograms).apply();
        prefs.edit().putBoolean("firstStart", false).apply();
        
        Button mainButtonInt0 = (Button) findViewById(R.id.mainButtonInt0);
        Button mainButtonInt1 = (Button) findViewById(R.id.mainButtonInt1);
        Button mainButtonInt2 = (Button) findViewById(R.id.mainButtonInt2);
        Button mainButtonInt3 = (Button) findViewById(R.id.mainButtonInt3);
        Button mainButtonInt4 = (Button) findViewById(R.id.mainButtonInt4);
        Button mainButtonInt5 = (Button) findViewById(R.id.mainButtonInt5);
        Button mainButtonInt6 = (Button) findViewById(R.id.mainButtonInt6);
        Button mainButtonInt7 = (Button) findViewById(R.id.mainButtonInt7);
        Button mainButtonInt8 = (Button) findViewById(R.id.mainButtonInt8);
        Button mainButtonInt9 = (Button) findViewById(R.id.mainButtonInt9);
		Button mainButtonSpace = (Button) findViewById(R.id.mainButtonSpace);
		Button mainButtonRight = (Button) findViewById(R.id.mainButtonRight);
		Button mainButtonLeft = (Button) findViewById(R.id.mainButtonLeft);
		Button mainButtonPlus = (Button) findViewById(R.id.mainButtonPlus);
		Button mainButtonMinus = (Button) findViewById(R.id.mainButtonMinus);
		
        int buttonWidth = getScreenWidth()/10;

        ViewGroup.LayoutParams lp = mainButtonInt9.getLayoutParams();
        lp.width = buttonWidth;
        mainButtonInt0.setLayoutParams(lp);
		mainButtonInt1.setLayoutParams(lp);
		mainButtonInt2.setLayoutParams(lp);
		mainButtonInt3.setLayoutParams(lp);
		mainButtonInt4.setLayoutParams(lp);
		mainButtonInt5.setLayoutParams(lp);
		mainButtonInt6.setLayoutParams(lp);
		mainButtonInt7.setLayoutParams(lp);
		mainButtonInt8.setLayoutParams(lp);
		mainButtonInt9.setLayoutParams(lp);

        mainButtonInt2.setTranslationX(buttonWidth * 2);
        mainButtonInt3.setTranslationX(buttonWidth * 3);
        mainButtonInt4.setTranslationX(buttonWidth * 4);
        mainButtonInt5.setTranslationX(buttonWidth * 5);
        mainButtonInt6.setTranslationX(buttonWidth * 6);
        mainButtonInt7.setTranslationX(buttonWidth * 7);
        mainButtonInt8.setTranslationX(buttonWidth * 8);
        mainButtonInt9.setTranslationX(buttonWidth * 9);
        mainButtonInt1.setTranslationX(buttonWidth);
		
		buttonWidth = getScreenWidth()/7;
        ((EditText) findViewById(R.id.mainEditText1)).setHorizontallyScrolling(true);
		ViewGroup.LayoutParams lp1= mainButtonSpace.getLayoutParams();
		lp1.width = buttonWidth*3;
		mainButtonSpace.setLayoutParams(lp1);
		
		ViewGroup.LayoutParams lp2 = mainButtonRight.getLayoutParams();
		lp2.width = buttonWidth;
		mainButtonRight.setLayoutParams(lp2);
		
		ViewGroup.LayoutParams lp3 = mainButtonLeft.getLayoutParams();
		lp3.width = buttonWidth;
		mainButtonLeft.setLayoutParams(lp3);
		

		ViewGroup.LayoutParams lp5 = mainButtonPlus.getLayoutParams();
		lp5.width = getScreenWidth()/11;
		
		mainButtonMinus.setLayoutParams(lp5);
		mainButtonMinus.setTranslationX(getScreenWidth()/11);
		
		Button mainButtonTimes = (Button) findViewById(R.id.mainButtonTimes);
		
		mainButtonTimes.setLayoutParams(lp5);
		mainButtonTimes.setTranslationX(getScreenWidth()/11*2);
		
		Button mainButtonDividedBy = (Button) findViewById(R.id.mainButtonDividedBy);
		
		mainButtonDividedBy.setLayoutParams(lp5);
		mainButtonDividedBy.setTranslationX(getScreenWidth()/11*3);
		
		Button mainButtonModulo = (Button) findViewById(R.id.mainButtonModulo);
		
		mainButtonModulo.setLayoutParams(lp5);
		mainButtonModulo.setTranslationX(getScreenWidth()/11*4);
		
		Button mainButtonNO = (Button) findViewById(R.id.mainButtonNO);
		
		mainButtonNO.setLayoutParams(lp5);
		mainButtonNO.setTranslationX(getScreenWidth()/11*5);
		
		mainButtonNO.getViewTreeObserver().addOnGlobalLayoutListener(
			new ViewTreeObserver.OnGlobalLayoutListener(){

				@Override
				public void onGlobalLayout() {
					// gets called after layout has been done but before display

					Button mainButtonInt9 = (Button) findViewById(R.id.mainButtonInt9);
					int sHeight = mainButtonInt9.getHeight();  
					
					RelativeLayout mainRelativeLayout1 = (RelativeLayout) findViewById(R.id.mainRelativeLayout1);
					int mHeight = mainRelativeLayout1.getHeight();
					
					EditText mainEditText1 = (EditText) findViewById(R.id.mainEditText1);
					ViewGroup.LayoutParams lp6 = mainEditText1.getLayoutParams();
					lp6.height = mHeight-(sHeight*5);
					mainEditText1.setLayoutParams(lp6);
					
				}

			});
		
		Button mainButtonBackQuote = (Button) findViewById(R.id.mainButtonBackQuote);
		mainButtonBackQuote.setLayoutParams(lp5);
		mainButtonBackQuote.setTranslationX(getScreenWidth()/11*6);
		
		Button mainButtonRandomDirection = (Button) findViewById(R.id.mainButtonRandomDirection);
		mainButtonRandomDirection.setLayoutParams(lp5);
		mainButtonRandomDirection.setText("?");
		mainButtonRandomDirection.setTranslationX(getScreenWidth()/11*7);
		
		Button mainButtonUnderscore = (Button) findViewById(R.id.mainButtonUnderscore);
		mainButtonUnderscore.setLayoutParams(lp5);
		mainButtonUnderscore.setTranslationX(getScreenWidth()/11*8);
		
		Button mainButtonPipe = (Button) findViewById(R.id.mainButtonPipe);
		mainButtonPipe.setLayoutParams(lp5);
		mainButtonPipe.setTranslationX(getScreenWidth()/11*9);
		
		Button mainButtonDoubleQuotes = (Button) findViewById(R.id.mainButtonDoubleQuotes);
		mainButtonDoubleQuotes.setLayoutParams(lp5);
		mainButtonDoubleQuotes.setText(String.valueOf(Character.toChars(34)));
		mainButtonDoubleQuotes.setTranslationX(getScreenWidth()/11*10);
		
		Button mainButtonColumn = (Button) findViewById(R.id.mainButtonColumn);
		ViewGroup.LayoutParams lp7 = mainButtonColumn.getLayoutParams();
		lp7.width = lp5.width;
		mainButtonColumn.setLayoutParams(lp7);
		
		Button mainButtonBackSlash = (Button) findViewById(R.id.mainButtonBackSlash);
		mainButtonBackSlash.setLayoutParams(lp7);
		mainButtonBackSlash.setText(String.valueOf(Character.toChars(92)));
		mainButtonBackSlash.setTranslationX(getScreenWidth()/11);
		
		Button mainButtonDollar = (Button) findViewById(R.id.mainButtonDollar);
		mainButtonDollar.setLayoutParams(lp7);
		mainButtonDollar.setTranslationX(getScreenWidth()/11*2);
		
		Button mainButtonDot = (Button) findViewById(R.id.mainButtonDot);
		mainButtonDot.setLayoutParams(lp7);
		mainButtonDot.setTranslationX(getScreenWidth()/11*3);
		
		Button mainButtonComma = (Button) findViewById(R.id.mainButtonComma);
		mainButtonComma.setLayoutParams(lp7);
		mainButtonComma.setTranslationX(getScreenWidth()/11*4);
		
		Button mainButtonLadder = (Button) findViewById(R.id.mainButtonLadder);
		mainButtonLadder.setLayoutParams(lp7);
		mainButtonLadder.setTranslationX(getScreenWidth()/11*5);
		
		Button mainButtonp = (Button) findViewById(R.id.mainButtonp);
		Button mainButtong = (Button) findViewById(R.id.mainButtong);
		mainButtonp.setLayoutParams(lp7);
		mainButtong.setLayoutParams(lp7);
		mainButtonp.setTranslationX(getScreenWidth()/11*6);
		mainButtong.setTranslationX(getScreenWidth()/11*7);
		
		Button mainButtonAmp = (Button) findViewById(R.id.mainButtonAmp);
		mainButtonAmp.setLayoutParams(lp7);
		mainButtonAmp.setTranslationX(getScreenWidth()/11*8);
		
		Button mainButtonInput = (Button) findViewById(R.id.mainButtonInput);
		Button mainButtonFinish = (Button) findViewById(R.id.mainButtonFinish);
		mainButtonInput.setLayoutParams(lp7);
		mainButtonFinish.setLayoutParams(lp7);
		mainButtonInput.setTranslationX(getScreenWidth() / 11 * 9);
		mainButtonFinish.setTranslationX(getScreenWidth() / 11 * 10);
		mainButtonFinish.setText("@");
        ViewGroup.LayoutParams lp8 = (findViewById(R.id.mainButtonDel)).getLayoutParams();
        lp8.width = getScreenWidth()/3;
        (findViewById(R.id.mainButtonDel)).setLayoutParams(lp8);
        (findViewById(R.id.mainButtonDel)).setOnLongClickListener(new Button.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setMessage("Do you want to clear your code ?")
                        .setTitle("Clear code")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ((EditText) findViewById(R.id.mainEditText1)).setText("");
                            }
                        })
                        .setNegativeButton("No", null);
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
        });
        ViewGroup.LayoutParams lp9 = (findViewById(R.id.mainButtonEnter)).getLayoutParams();
        lp9.width = getScreenWidth()/3;
        (findViewById(R.id.mainButtonEnter)).setLayoutParams(lp9);
        Button mainButtonRun = (Button) findViewById(R.id.mainButtonRun);
        mainButtonRun.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent i = new Intent(v.getContext(), RunActivity.class);
                i.putExtra("code", getCode(((EditText) findViewById(R.id.mainEditText1)).getText().toString()));
                i.putExtra("size", getSize(((EditText) findViewById(R.id.mainEditText1)).getText().toString()));
                startActivity(i);
            }

            private String getCode(String code)
            {
                String[] lines = code.split("\n");
                int max = 0;
                for (String line : lines) {
                    max = Math.max(max, line.length());
                }
                max++;
                for(int i=0;i<lines.length;i++){
                    while(lines[i].length() < max){
                        lines[i] += " ";
                    }
                }
                String joined = "";
                for(String line: lines){
                    joined += line + "\n";
                }
                return joined;
            }
            private int[] getSize(String code)
            {
                String[] lines = code.split("\n");
                int width = 0;
                for (String line : lines) {
                    width = Math.max(width, line.length());
                }
                width++;
                return new int[] {width, lines.length};
            }
        });
        Button[] buttonArray = new Button[]{
                mainButtonInt0,
                mainButtonAmp,
                mainButtonBackQuote,
                mainButtonBackSlash,
                mainButtonColumn,
                mainButtonComma,
                mainButtonDividedBy,
                mainButtonDollar,
                mainButtonDot,
                mainButtonDoubleQuotes,
                mainButtonFinish,
                mainButtong,
                mainButtonInput,
                mainButtonInt1,
                mainButtonInt2,
                mainButtonInt3,
                mainButtonInt4,
                mainButtonInt5,
                mainButtonInt6,
                mainButtonInt7,
                mainButtonInt8,
                mainButtonInt9,
                mainButtonLadder,
                mainButtonLeft,
                mainButtonMinus,
                mainButtonModulo,
                mainButtonNO,
                mainButtonp,
                mainButtonPipe,
                mainButtonPlus,
                mainButtonRandomDirection,
                mainButtonRight,
                mainButtonSpace,
                mainButtonTimes,
                mainButtonUnderscore,
                (Button) findViewById(R.id.mainButtonUp),
                (Button) findViewById(R.id.mainButtonDown),
                (Button) findViewById(R.id.mainButtonDel),
                (Button) findViewById(R.id.mainButtonEnter)
        };

        for(Button b: buttonArray){
            b.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View button) {
                    EditText mainEditText = (EditText) findViewById(R.id.mainEditText1);

                    int position = mainEditText.getSelectionStart()+1;
                    CharSequence ch = (((Button) button).getText());
                    if(button == findViewById(R.id.mainButtonDel)){
                        try{
                            //mainEditText.setText(mainEditText.getText().subSequence(0, mainEditText.getText().length()-1));
                            if(position-1 == mainEditText.length()){
                                mainEditText.setText(mainEditText.getText().subSequence(0, mainEditText.getText().length()-1));
                                mainEditText.setSelection(mainEditText.length());
                                return;
                            }
                            SpannableStringBuilder selectedStr=new SpannableStringBuilder(mainEditText.getText());
                            selectedStr.replace(position-2, position-1, "");
                            mainEditText.setText(selectedStr);
                            mainEditText.setSelection(position-2);
                            return;
                        }catch(Exception ignored){
                            return;
                        }
                    }
                    if(button == findViewById(R.id.mainButtonEnter)){
                        //mainEditText.append(String.valueOf(Character.toChars(10)));
                        mainEditText.setText(mainEditText.getText().insert(mainEditText.getSelectionStart(), String.valueOf(Character.toChars(10))));
                        ch = "";
                    }
                    mainEditText.setText(mainEditText.getText().insert(mainEditText.getSelectionStart(), ch));
                    /*if(((Button) button).getText() == ""){
                        mainEditText.setText(mainEditText.getText().insert(mainEditText.getSelectionStart(), String.valueOf(Character.toChars(32))));
                    }*/
                    try{
                        //Cannot do inline because position is overridden before getting its value.
                        //Toast.makeText(button.getContext(), "Selection : " + mainEditText.getSelectionStart() + ", " + mainEditText.getSelectionEnd(), Toast.LENGTH_SHORT).show();
                        mainEditText.setSelection(position);
                    } catch(IndexOutOfBoundsException ignored){

                    }
                }
            });
        }

    }
    
    public int getScreenWidth(){
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }
	@Override
	 public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.menu_main, menu);
	return true;
}
 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
        /*if (id == R.id.action_keyboard) {
            //TODO: Fix this. The goal is to still get a cursor, but not show the keyboard.
            EditText et = (EditText) findViewById(R.id.mainEditText1);
            et.setFocusable(!et.isFocusable());
            return true;
        }*/
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            intent.putExtra( PreferenceActivity.EXTRA_SHOW_FRAGMENT, SettingsActivity.GeneralPreferenceFragment.class.getName());
            intent.putExtra( PreferenceActivity.EXTRA_NO_HEADERS, true );
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_samples) {
            //TODO: 'Sample & saved' activity
            Intent intent = new Intent(this, SavedActivity.class);
            startActivityForResult(intent, SAMPLE_RESULT);
            return true;
        }
        if (id == R.id.action_save) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            View rootView =inflater.inflate(R.layout.save_code_layout, null);
            final EditText editText = (EditText) rootView.findViewById(R.id.editText);
            final EditText editTextCode = (EditText) this.findViewById(R.id.mainEditText1);

            builder.setView(rootView);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    String name = editText.getText().toString();
                    String code = editTextCode.getText().toString();
                    CodeSavingUtils.saveCode(name, code, getApplicationContext());
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //Don't do anything
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }
        if (id == R.id.action_specifications) {
            Intent intent = new Intent(this, LanguageSpecifications.class);
            startActivity(intent);
            return true;
        }
		return super.onOptionsItemSelected(item);
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == SAMPLE_RESULT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                EditText editTextCode = (EditText) this.findViewById(R.id.mainEditText1);
                String code = data.getExtras().getString("resultCode", "");
                editTextCode.setText(code);
            }if (resultCode == RESULT_RELOAD) {
                Intent intent = new Intent(this, SavedActivity.class);
                startActivityForResult(intent, SAMPLE_RESULT);
            }
        }
    }
}
