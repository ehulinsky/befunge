package com.fliife.befunge;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;
import java.util.Stack;


public class RunActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run);
        Intent i = this.getIntent();
        CharSequence code = i.getCharSequenceExtra("code");
        int[] size = i.getIntArrayExtra("size");

        updateUI(code.toString(), 1);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int delay = Integer.parseInt(prefs.getString("default_slowdown", "20"));
        final InterpreterRunnable runnableCode = new InterpreterRunnable(code, size, delay);
        final Button runButtonMain = (Button) findViewById(R.id.runButtonMain);
        final Button runPauseButton = (Button) findViewById(R.id.runPauseButton);
        final Button runSlowButton = (Button) findViewById(R.id.runSlowButton);


        final ThreadObject interpreter = new ThreadObject(new Thread(runnableCode), runnableCode);
        runButtonMain.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = interpreter.getThread();
                if(thread.isAlive()){
                    thread.interrupt();
                    ((Button) v).setText("START");
                    interpreter.setThread(new Thread(runnableCode), runnableCode);
                    runPauseButton.setText("PAUSE");
                    interpreter.resume();
                }else{
                    ((EditText) findViewById(R.id.runEditTextOutput)).setText("");
                    //Log.v("ThreadStatus", "Thread is " + ((thread.isAlive())?"":"not ") + "running");

                    try{
                        thread.start();
                    }catch(IllegalThreadStateException e){
                        e.printStackTrace();

                        interpreter.setThread(new Thread(runnableCode), runnableCode);
                        thread = interpreter.getThread();
                        thread.start();
                    }
                    ((Button)v).setText("STOP");
                }

            }
        });
        runPauseButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!interpreter.isSuspended){
                    interpreter.suspend();
                    ((Button) v).setText("RESUME");
                }else{
                    interpreter.resume();
                    ((Button)v).setText("PAUSE");
                }
            }
        });
        runSlowButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v){
                AlertDialog.Builder builder = new AlertDialog.Builder(RunActivity.this);
                LayoutInflater inflater = RunActivity.this.getLayoutInflater();
                View rootView =inflater.inflate(R.layout.slowdown_picker_layout, null);
                final SeekBar seekBar = (SeekBar) rootView.findViewById(R.id.seekBar);
                final TextView text = (TextView) rootView.findViewById(R.id.textView);
                text.setText("Slowdown (in ms) : " + interpreter.getDelay());
                seekBar.setProgress(interpreter.getDelay());
                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        text.setText("Slowdown (in ms) : " + progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                builder.setView(rootView);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        interpreter.setDelay(seekBar.getProgress());
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Don't do anything
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }
    class ThreadObject {
        private Thread thread = null;
        public boolean isSuspended = false;
        public InterpreterRunnable interpreterRunnable;

        public ThreadObject(Thread thread, InterpreterRunnable runnable) {
            this.thread = thread;
            this.interpreterRunnable = runnable;
        }

        public Thread getThread() {
            return this.thread;
        }

        public void setThread(Thread thread, InterpreterRunnable runnable) {
            this.thread = thread;
            this.interpreterRunnable = runnable;
        }

        public void setDelay(int delay){
            interpreterRunnable.setDelay(delay);
        }
        public int getDelay(){
            return interpreterRunnable.getDelay();
        }

        public void suspend() {
            interpreterRunnable.pause();
            this.isSuspended = true;
        }

        public void resume() {
            interpreterRunnable.resume();
            this.isSuspended = false;
        }
    }

    class InterpreterRunnable implements Runnable{
        private final int[] size;
        private boolean doPause = false;
        private CharSequence code = "";
        private int delay;
        public InterpreterRunnable(CharSequence code, int[] size, int delay){
            this.code = code;
            this.size = size;
            this.delay = delay;
        }

        public void pause(){
            this.doPause = true;
        }

        public boolean isPaused(){
            return this.doPause;
        }

        public void setDelay(int delay){
            this.delay = delay;
        }

        public void resume(){
            this.doPause = false;
        }

        @Override
        public void run() {

            runCode(code, size[0], size[1], this);
        }

        public int getDelay() {
            return delay;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_run, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("ConstantConditions")
    public void runCode(CharSequence code, int width, int height, InterpreterRunnable context) {
        final Stack<Integer> stack = new Stack<>();
        int posX, posY, direction;
		final String[][] box = new String[height][width];
		for (int i=0; i < box.length; i++) {
			for (int j=0; j < box[i].length; j++) {
				box[i][j] = " ";
			}
		}
		final String scode = code.toString();
        final EditText et = (EditText) findViewById(R.id.runEditTextOutput);
        final EditText stackEditText = (EditText) findViewById(R.id.runEditTextStack);

        String[] rows = scode.split("\n");
        int r = 0;
        for (String row : rows) {
            //r was being incremented before, thus making the index wrong
            String[] line = row.split("");
            box[r] = line;
            r++;
        }
        posX = 0;
        posY = 1;
        direction = 1;
        int a, b, v, x, y;
        boolean stringMode = false;
        boolean skipNext = false;
        EditText eti = (EditText) findViewById(R.id.runEditTextInput);
        String input = eti.getText().toString();
        String output = "";
        Boolean doSwitch = true;

        /*
        String finalCode = "";
        for (String[] s1: box) {
            if (s1.length == 0 || s1 == null) {
                //continue;
            }
            for (String s2: s1) {
                if (s2 == null) {
                    //continue;
                }
                finalCode += s2;
            }
            finalCode += System.getProperty("line.separator");
        }*/

		boolean doRun = true;
        boolean doPause;
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

        int minDelay = Integer.parseInt(pref.getString("min_delay", "20"));
        //TODO: Stepping. Temp boolean (same as pause)
        while (doRun) {
            doPause = context.isPaused();
            if(!doPause){
                int index;
                doRun = !Thread.currentThread().isInterrupted();
                String s;
                try {
                    s = box[posX][posY];
                } catch (ArrayIndexOutOfBoundsException ignored) {
                    s = " ";
                }
                if (skipNext) {
                    skipNext = false;
                    doSwitch = false;
                }

                if (s.isEmpty()) {
                    doSwitch = false;
                }

                updateUI(box, posX, posY);

                //Log.v("runCode()", "current char : " + s);
                if (s.contains("\"")) {
                    stringMode = !stringMode;
                    //Log.v("runCode()", "stringMode is now " + stringMode);
                    //posY++;
                    doSwitch = false;
                }
                if (stringMode && !s.contains("\"")) {
                    stack.push(s.hashCode());
                    //posY++;
                    doSwitch = false;
                }
                if (doSwitch) {
                    try {
                        switch (s) {
                            case "0":
                                stack.push(0);
                                break;
                            case "1":
                                stack.push(1);
                                break;
                            case "2":
                                stack.push(2);
                                break;
                            case "3":
                                stack.push(3);
                                break;
                            case "4":
                                stack.push(4);
                                break;
                            case "5":
                                stack.push(5);
                                break;
                            case "6":
                                stack.push(6);
                                break;
                            case "7":
                                stack.push(7);
                                break;
                            case "8":
                                stack.push(8);
                                break;
                            case "9":
                                stack.push(9);
                                break;
                            case ">":
                                direction = 1;
                                break;
                            case "<":
                                direction = -1;
                                break;
                            case "v":
                                direction = 2;
                                break;
                            //Log.v("runCode().switch(s).case \"v\"", "Down ?");
                            case "^":
                                direction = -2;
                                break;
                            case "@":
                                direction = 0;
                                break;
                            case "+":
                                a = safePop(stack);
                                b = safePop(stack);
                                stack.push(a + b);
                                break;
                            case "-":
                                a = safePop(stack);
                                b = safePop(stack);
                                stack.push(b - a);
                                break;
                            case " ":
                                break;
                            case "*":
                                a = safePop(stack);
                                b = safePop(stack);
                                stack.push(a * b);
                                break;
                            case "/":
                                a = safePop(stack);
                                b = safePop(stack);
                                stack.push((int) Math.floor(b / a));
                                break;
                            case "%":
                                a = safePop(stack);
                                b = safePop(stack);
                                if (a != 0 && b != 0) {
                                    stack.push(b % a);
                                }
                                break;
                            case "!":
                                a = safePop(stack);
                                if (a == 0) {
                                    stack.push(1);
                                } else {
                                    stack.push(0);
                                }
                                break;
                            case "`":
                                a = safePop(stack);
                                b = safePop(stack);
                                if (b > a) {
                                    stack.push(1);
                                } else {
                                    stack.push(0);
                                }
                                break;
                            case "?":
                                //HAS NOT TO BE 0
                                direction = randInt(-2, 2);
                                while (direction == 0) direction = randInt(-2, 2);
                                break;
                            case "_":
                                if (safePop(stack) == 0) {
                                    direction = 1;
                                } else {
                                    direction = -1;
                                }
                                break;
                            case "|":
                                if (safePop(stack) == 0) {
                                    direction = 2;
                                } else {
                                    direction = -2;
                                }
                                break;
                            case ":":
                                int pop=safePop(stack);
                                stack.push(pop);
                                stack.push(pop);
                                break;
                            case "\\":
                                a = safePop(stack);
                                b = safePop(stack);
                                stack.push(a);
                                stack.push(b);
                                break;
                            case "$":
                                safePop(stack);
                                break;
                            case ".":
                                output = safePop(stack) + "";
                                break;
                            case ",":
                                output = String.valueOf(Character.toChars(safePop(stack)));
                                break;
                            case "#":
                                skipNext = true;
                                break;
                            case "p":
                                y = safePop(stack);
                                x = safePop(stack)+1;
                                v = safePop(stack);

                                //makes sure v is a valid ASCII character
                                while(v>255)
                                {
                                    v-=255;
                                }
                                while(v<0)
                                {
                                    v+=255;
                                }
                                box[y][x] = String.valueOf(Character.toChars(v));
                                updateUI(box,x-1,y);

                                break;
                            case "g":
                                y = safePop(stack);
                                x = safePop(stack)+1;

                                stack.push(box[y][x].hashCode());
                                break;
                            case "&":
                                String[] nums=input.split(",");
                                StringBuilder stringBuilder=new StringBuilder();

                                try {
                                    stack.push(Integer.parseInt(nums[0]));
                                    input="";
                                    for(int i=1;i<nums.length-1;i++)
                                    {
                                        stringBuilder.append(nums[i]).append(",");
                                    }
                                    if(nums.length>1) {
                                        stringBuilder.append(nums[nums.length - 1]);
                                    }
                                    input=stringBuilder.toString();
                                } catch (Exception ignore) {}
                                break;
                            case "~":
                                try {
                                    stack.push((int) input.charAt(0));
                                    input = input.substring(1);
                                }catch (IndexOutOfBoundsException ignored){
                                    stack.push(-1);
                                }

                                break;
                            default:
                                //Ignore error in order to avoid weird ascii chars. Sometimes just randomly detect an 'à' char (160)
                                output = "";
                                break;
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        doRun = false;
                    }
                }

                doSwitch = true;
                //Log.v("RunActivity", "Finished switch, out = " +output);
                final String finalOutput = output;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        stackEditText.setText(stack.toString());
                        et.append(finalOutput);
                    }
                });
                output = "";
                switch (direction) {
                    case -1:
                        posY--;
                        if (posY == 0) {
                            posY = width;
                        }
                        break;
                    case -2:
                        posX--;
                        if (posX == -1) {
                            posX = height;
                        }
                        break;
                    case 0:
                        break;
                    case 2:
                        posX++;
                        if (posX == height + 1) {
                            posX = 0;
                        }
                        break;
                    case 1:
                        posY++;
                        if (posY == width + 1) {
                            posY = 1;
                        }
                        break;
                    default:
                        posY++;
                }
                if (direction == 0) {
                    break;
                }
                //posY++;
                try {
                    Thread.sleep((context.delay<minDelay)?minDelay:context.delay);
                } catch (InterruptedException ie) {
                    doRun = false;
                }
                //break;
            }
        }
        Log.v("Interpreter", "Finished");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((Button)findViewById(R.id.runButtonMain)).setText("START");
            }
        });
    }

    //returns 0 if there is nothing in stack, as documentation says
    private Integer safePop(Stack<Integer> stack) {
        if(stack.isEmpty())
        {
            return 0;
        }
        else
        {
            return stack.pop();
        }

    }

    private int getIndex(int width, int posX, int posY) {
        int index = posY + (posX) * width;
        return index;
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    public Spannable colorAtIndex(String str,int index) {

        Spannable colored = new SpannableString(str.replaceAll("(?<=\\n)[\\s\\n]+$", ""));
        int b;
        b = index-1;
        colored.setSpan(new ForegroundColorSpan(Color.WHITE), b, b + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        colored.setSpan(new BackgroundColorSpan(Color.BLACK), b, b + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return colored;
    }
    public void updateUI(final String[][] box,final int xPos, final int yPos) {
        runOnUiThread(new Runnable() {
				@Override
				public void run() {
					try {
                        String codeString = "";
                        StringBuilder s=new StringBuilder(codeString);
                        for (String[] s1: box) {
                            for (String s2: s1) {
                                if(s2.length()>0) {
                                    if(s2.charAt(0)>32) {
                                        s.append(s2);
                                    }
                                    else
                                    {
                                        s.append(" ");
                                    }

                                }
                            }
                            s.append(System.getProperty("line.separator"));
                        }


                        EditText runEditTextCode = (EditText) findViewById(R.id.runEditTextCode);
                        runEditTextCode.setText(
                                colorAtIndex(
                                        s.toString(),
                                        getIndex(box[0].length,xPos,yPos))
                        );
                    }catch (Exception e){
                        e.printStackTrace();
                    }

				}
			});
    }

    public void updateUI(final String codeString,final int index) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    EditText runEditTextCode = (EditText) findViewById(R.id.runEditTextCode);
                    runEditTextCode.setText(
                            colorAtIndex(
                                    codeString,
                                    index)
                    );
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });
    }

   /* public void changeChar(final String fullCode, final int index,final char newChar) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String adjustedFullCode = fullCode.replaceAll("(?<=\\n)[\\s\\n]+$", "");
                    EditText runEditTextCode = (EditText) findViewById(R.id.runEditTextCode);

                    adjustedFullCode=adjustedFullCode.substring(0,index)+newChar+adjustedFullCode.substring(index+1);

                    runEditTextCode.setText(adjustedFullCode);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }*/
}
